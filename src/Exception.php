<?php

namespace daVerona\Texting\Base;

class TextingException extends Exception {
  const MESSAGE_AUTHENTICATION_FAILED = ["code" => 101, "message" => "Authentication failed"];
  const MESSAGE_PERMISSION_DENIED = ["code" => 102, "message" => "Permission denied"];
  const MESSAGE_MISSING_ARGUMENT = ["code" => 201, "message" => "Missing argument"];
  const MESSAGE_INVALID_ARGUMENT = ["code" => 202, "message" => "Invalid argument"];
  const MESSAGE_UNKNOWN_METHOD = ["code" => 203, "message" => "Unknown method"];
  const MESSAGE_INVALID_PHONENUMBER = ["code" => 204, "message" => "Invalid phone number"];
  const MESSAGE_RECORD_NOT_FOUND = ["code" => 301, "message" => "Record not found"];
  const MESSAGE_NOT_ENOUGH_CREDIT = ["code" => 801, "message" => "Not enough credit"];
  const MESSAGE_UNDEFINED_ERROR = ["code" => 901, "message" => "Undefined error"];
  const MESSAGE_UNKNOWN_ERROR = ["code" => 902, "message" => "Unknown error"];
  const MESSAGE_ERROR_OCCURRED = ["code" => 903, "message" => "Error occurred"];

  protected $providerMessage;
  protected $providerCode;

  public function __construct($index, $providerMessage="", $providerCode=0, $previous=null) {
    $message = $index["message"];
    $code = $index["code"];
    $this->providerMessage = $providerMessage;
    $this->providerCode = $providerCode;
    parent::__construct($message, $code, $previous);
  }

  public function getProviderMessage() {
    return $this->providerMessage;
  }

  public function getProviderCode() {
    return $this->providerCode;
  }
}

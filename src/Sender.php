<?php

namespace daVerona\Texting\Base;

abstract class Sender {
  protected $credentials = array();
  protected $options = array();

  abstract public function __construct($credentials, $options);
  abstract public function signin($options);
  abstract public function signout($options);
  abstract public function dryrun($data);
  abstract public function send($data);
  abstract public function cancel($data);
  abstract public function stats($options);
}
